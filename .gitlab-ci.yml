include:
  - project: computing/gitlab-ci-templates
    file:
      - debian.yml
      - python.yml
      - rhel.yml

stages:
  - dist
  - source
  - build
  - test
  - lint
  - docs
  - deploy

# -- macros

.el7:
  image: igwn/base:el7-testing
  variables:
    EPEL: "true"

.el8:
  image: igwn/base:el8-testing
  variables:
    EPEL: "true"
  allow_failure: true

.buster:
  image: igwn/base:buster

.bullseye:
  image: igwn/base:bullseye
  allow_failure: true

# -- dist -------------------
#
# This job makes the gwdatafind-X.Y.Z.tar.gz
# distribution and uploads it as a job
# artifact
#

tarball:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:build
    - .python:build
  image: python:3
  stage: dist
  variables:
    WHEEL: "false"

# -- source packages --------
#
# These jobs make src RPMs
#

.source:
  stage: source
  needs:
    - tarball
  variables:
    TARBALL: "lscsoft-glue*.tar.*"

.srpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:srpm
    - .rhel:srpm
    - .source

srpm:el7:
  extends:
    - .srpm
    - .el7

srpm:el8:
  extends:
    - .srpm
    - .el8

.dsc:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:dsc
    - .debian:dsc
    - .source

dsc:buster:
  extends:
    - .dsc
    - .buster

dsc:bullseye:
  extends:
    - .dsc
    - .bullseye

# -- binary packages --------
#
# These jobs generate binary RPMs
# from the src RPMs
#

.binary:
  stage: build

.rpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:rpm
    - .rhel:rpm
    - .binary

rpm:el7:
  extends:
    - .rpm
    - .el7
  needs:
    - srpm:el7

rpm:el8:
  extends:
    - .rpm
    - .el8
  needs:
    - srpm:el8

.deb:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:deb
    - .debian:deb
    - .binary
  variables:
    DSC: "lscsoft-glue*.dsc"

deb:buster:
  extends:
    - .deb
    - .buster
  needs:
    - dsc:buster

deb:bullseye:
  extends:
    - .deb
    - .bullseye
  needs:
    - dsc:bullseye

# -- test -------------------
#
# These jobs run the tests on
# all supported platforms
#

# -- test with pip

.test:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:test
    - .python:test
  stage: test
  needs:
    - tarball
  variables:
    # don't need the git repo
    GIT_STRATEGY: none
    # default to PYTHON3
    PYTHON: "python3"
    # command to use for coverage
    COVERAGE: "${PYTHON} -m coverage run --omit \"*.so\" --parallel-mode --source glue"
  before_script:
    # test requirements
    - ${PYTHON} -m pip install
          "coverage>=4.1"
          lalsuite
    # install this project
    - ${PYTHON} -m pip install lscsoft-glue-*.tar.*
  script:
    # extract tests from tarball
    - tar -xf lscsoft-glue*.tar.* --wildcards --strip-components 1 */test
    # run tests
    - make -C test check -j 4 PYTHON="${COVERAGE}"
  after_script:
    # combine coverage from different processes
    - ${PYTHON} -m coverage combine test/.coverage*
    # run the usual after_script
    - !reference [".python:test", after_script]

test:python3.5:
  extends:
    - .test
  image: python:3.5

test:python3.6:
  extends:
    - .test
  image: python:3.6

test:python3.7:
  extends:
    - .test
  image: python:3.7

test:python3.8:
  extends:
    - .test
  image: python:3.8

test:python3.9:
  extends:
    - .test
  image: python:3.9

# -- test RHEL

.test:el:
  extends:
    - .test
  before_script:
    # set up yum caching
    - !reference [".rhel:base", before_script]
    # configure EPEL
    - yum -y -q install epel-release && yum -y -q install epel-rpm-macros
    # install testing dependencies
    - if [[ "${PYTHON}" == "python3"* ]]; then
          PYX=$(rpm --eval '%{?python3_pkgversion:%{python3_pkgversion}}%{!?python3_pkgversion:3}');
      else
          PYX="2";
      fi
    - yum -y -q install
          make
          python${PYX}-lal
          python${PYX}-matplotlib
          python${PYX}-pip
    - yum -y install python${PYX}-*glue*.rpm
    # force a good enough version of coverage.py
    - ${PYTHON} -m pip install "coverage >=4.1"

test:el7:
  extends:
    - .test:el
    - .el7
  needs:
    - tarball
    - rpm:el7

test:el7:python2:
  extends:
    - test:el7
  variables:
    PYTHON: "python2"

test:el8:
  extends:
    - .test:el
    - .el8
  needs:
    - tarball
    - rpm:el8

# -- test Debian

.test:debian:
  extends:
    - .test
    - .debian:base
  before_script:
    # set up apt
    - !reference [".debian:base", before_script]
    - apt-get -y -q -q install dpkg
    # install testing dependencies
    - apt-get -y -q install
          make
          python3-coverage
          python3-lal
          python3-pip
    # install our package(s)
    - dpkg --install *.deb || { apt-get -q -y -f install; dpkg -i python${PYX}-*.deb; }

test:buster:
  extends:
    - .test:debian
    - .buster
  needs:
    - tarball
    - deb:buster

test:bullseye:
  extends:
    - .test:debian
    - .bullseye
  needs:
    - tarball
    - deb:bullseye

# -- lint -------------------
#
# These jobs check the code
# for quality issues
#

.lint:
  stage: lint

flake8:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:flake8
    - .python:flake8
    - .lint
  needs: []
  allow_failure: true
  # ... for now

.rpmlint:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:lint
    - .rhel:lint
    - .lint
  variables:
    GIT_STRATEGY: fetch
    RPMLINT_OPTIONS: '--info --file .rpmlintrc'

rpmlint:el7:
  extends:
    - .rpmlint
    - .el7
  needs:
    - rpm:el7

rpmlint:el8:
  extends:
    - .rpmlint
    - .el8
  needs:
    - rpm:el8

.lintian:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/debian/#.debian:lint
    - .debian:lint
    - .lint
  variables:
    LINTIAN_OPTIONS: "--color always --allow-root --pedantic"

lintian:buster:
  extends:
    - .lintian
    - .buster
  needs:
    - deb:buster

lintian:bullseye:
  extends:
    - .lintian
    - .bullseye
  needs:
    - deb:bullseye

# -- docs -------------------
#
# This job generates the
# project documentation using
# sphinx-apidoc
#

docs:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:sphinx
    - .python:sphinx
  stage: docs
  image: python:3.8
  needs: []
  variables:
    REQUIREMENTS: "sphinx_rtd_theme"
  before_script:
    - !reference [".python:sphinx", before_script]
    # install glue and lalsuite (everything else is in setup.py)
    - python -m pip install . lalsuite
  script:
    # generate docs skeleton
    - glue_version=$(python setup.py --version)
    - python -m sphinx.ext.apidoc
          --module-first
          --separate
          --full
          --ext-autodoc
          --ext-intersphinx
          --doc-project "lscsoft-glue"
          --doc-author "The LIGO Scientific Collaboration and The Virgo Collaboration"
          --doc-version "${glue_version}"
          --output-dir ${SOURCEDIR}
          glue
    # use module page as index
    - mv -v docs/glue.rst docs/index.rst
    # use sphinx_rtd_theme
    - sed -i 's/alabaster/sphinx_rtd_theme/g' docs/conf.py
    # run sphinx to generate docs
    - !reference [".python:sphinx", script]

pages:
  stage: deploy
  needs:
    - docs
  only:
    - tags
  script:
    - mv html public
  artifacts:
    paths:
      - public
